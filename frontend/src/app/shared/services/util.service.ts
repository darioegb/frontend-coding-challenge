import { Injectable } from '@angular/core';
import { ObjectIndexer } from '../models/object-indexer.model';
import { SortOption } from '../models/table.model';

@Injectable({
  providedIn: 'root',
})
export class UtilService {
  /**
   * 💡 Sort array list by array field. Also can set direction with reverse flag
   * @param items T[]
   * @param option SortOption
   *@example { key: 'name', reverse: true }
   * @returns T[]
   */
  sortBy<T extends ObjectIndexer>(
    items: T[],
    { key, reverse }: SortOption<T>
  ): T[] {
    let sortItems = [...items].sort((a, b) =>
      a[key]
        ?.toString()
        .toLowerCase()
        .localeCompare(b[key]?.toString().toLowerCase())
    );
    return reverse ? [...sortItems].reverse() : sortItems;
  }
}
