export interface Column<T> {
  headerDef: keyof T;
  cellDef: string;
  columnType: ColumnType;
  columnWidth?: string;
  columnColor?: string;
  columnTextAlign?: ColumnTextAlign;
  noZeroOrEmpty?: boolean;
}

export interface Sort<T> {
  order: OrderType;
  column?: keyof T;
}

export interface ColumnTextAlign {
  header?: string;
  body?: string;
}

export enum OrderType {
  Ascending = 1,
  Descending = 2,
  None = 3,
}

export type SortOption<T> = {
  key: keyof T;
  reverse?: boolean;
};

export type ColumnType = 'string' | 'number' | 'percentage';
