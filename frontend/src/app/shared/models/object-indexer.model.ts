export interface ObjectIndexer {
  [key: string]: any;
}
