import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';

import { TableComponent } from './components/table/table.component';
import { TableItemComponent } from './components/table-item/table-item.component';

@NgModule({
  declarations: [TableComponent, TableItemComponent],
  imports: [CommonModule, TranslateModule.forChild()],
  exports: [TableComponent, TableItemComponent, TranslateModule],
})
export class SharedModule {}
