import { Component, Input } from '@angular/core';
import { ObjectIndexer } from '../../models/object-indexer.model';

import { Column } from '../../models/table.model';

@Component({
  selector: 'app-table-item',
  templateUrl: './table-item.component.html',
  styleUrls: ['./table-item.component.scss'],
})
export class TableItemComponent<T extends ObjectIndexer> {
  @Input() column!: Column<T>;
  @Input() item!: T;
}
