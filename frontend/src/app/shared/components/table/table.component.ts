import { Component, EventEmitter, Input, Output } from '@angular/core';
import {
  Column,
  ColumnTextAlign,
  OrderType,
  Sort,
} from '../../models/table.model';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss'],
})
export class TableComponent<T> {
  @Input() columns: Column<T>[] = [];
  @Input() dataSource: T[] = [];
  @Output() sort: EventEmitter<Sort<T>> = new EventEmitter();
  @Input() firstItem?: T;
  @Input() lastItem?: T;
  @Input() defaultSortField?: keyof T;
  currentSort: Sort<T> = { column: undefined, order: OrderType.None };

  changeOrder(headerDef: keyof T) {
    let order = this.getOrder(headerDef);
    this.currentSort = { column: headerDef, order };
    if (this.checkNoneOrder()) {
      this.currentSort.column = this.defaultSortField
        ? this.defaultSortField
        : undefined;
    }
    this.sort.emit(this.currentSort);
  }

  getOrder(headerDef: keyof T): OrderType {
    if (this.currentSort.column === headerDef) {
      if (this.checkDescendingOrder()) {
        return OrderType.None;
      }
      return this.checkAscendingOrder()
        ? OrderType.Descending
        : OrderType.Ascending;
    } else {
      return OrderType.Ascending;
    }
  }

  getSortClass(column: keyof T): string {
    let sortClass = this.checkDescendingOrder() ? 'desc' : 'asc';
    return column === this.currentSort.column ? sortClass : '';
  }

  getColumnWidthClass(columnWidth: string | undefined): string {
    return columnWidth ? columnWidth : 'col-md';
  }

  getColumnTextAlignClass(
    columnTextAlign: ColumnTextAlign,
    isHeader = false
  ): string  {
    if (columnTextAlign.body) {
      return columnTextAlign.body;
    }
    return columnTextAlign.header && isHeader ? columnTextAlign.header : '';
  }

  getColumnColorClass(columnColor: string | undefined): string {
    return columnColor ? columnColor : '';
  }

  private checkAscendingOrder(): boolean {
    return this.currentSort.order === OrderType.Ascending;
  }

  private checkDescendingOrder(): boolean {
    return this.currentSort.order === OrderType.Descending;
  }

  private checkNoneOrder(): boolean {
    return this.currentSort.order === OrderType.None;
  }
}
