import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'labourCostReport', pathMatch: 'full' },
  {
    path: 'labourCostReport',
    loadChildren: () =>
      import('./modules/labour-cost-report/labour-cost-report.module').then(
        (m) => m.LabourCostReportModule,
      ),
  },
  { path: '**', redirectTo: 'labourCostReport' }, // NOT FOUND
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { relativeLinkResolution: 'legacy' })],
  exports: [RouterModule],
})
export class AppRoutingModule {}
