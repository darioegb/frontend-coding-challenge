export interface LabourStats {
  directContractors?: Provider;
  providers: Provider[];
  total: Provider;
}

export interface Provider {
  complianceScorePercentage: number;
  grossPayTotal: number;
  labourCostTotal: number;
  name: string;
  payrollAdminTotal: number;
  providerId: number;
  workerCount: number;
  workForcePercentage: number;
}
