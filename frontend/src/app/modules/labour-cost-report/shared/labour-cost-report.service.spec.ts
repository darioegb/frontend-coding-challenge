import { TestBed } from '@angular/core/testing';

import { LabourCostReportService } from './labour-cost-report.service';

describe('LabourCostReportService', () => {
  let service: LabourCostReportService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(LabourCostReportService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
