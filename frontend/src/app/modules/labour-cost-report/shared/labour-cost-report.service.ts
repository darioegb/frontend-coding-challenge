import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';
import { map } from 'rxjs/operators';

import { environment } from 'src/environments/environment';
import { LabourStats } from './labour-cost-report.model';

@Injectable({
  providedIn: 'root',
})
export class LabourCostReportService {
  baseUrl = `${environment.apiUrl}/labourstats`;

  constructor(private http: HttpClient) {}

  getAll(): Observable<LabourStats> {
    return this.http.get<LabourStats>(this.baseUrl).pipe(
      map((data: any) => {
        const fistItem = data[0];
        const total = fistItem.total[0];
        total.complianceScorePercentage = total.complianceStats?.Total || 0;
        total.workForcePercentage = 100;
        const directContractors = fistItem.directContractors[0];
        directContractors.complianceScorePercentage =
          directContractors.complianceStats?.Total || 0;
        directContractors.workForcePercentage =
          directContractors.complianceScorePercentage
            ? (directContractors.workerCount / total.workerCount) * 100
            : 0;
        const providers = fistItem.providers;
        fistItem.providers?.forEach((provider: any) => {
          provider.complianceScorePercentage =
            provider.complianceStats?.Total || 0;
          provider.workForcePercentage = provider.complianceScorePercentage
            ? (provider.workerCount / total.workerCount) * 100
            : 0;
          return provider;
        });
        return { directContractors, total, providers } as LabourStats;
      })
    );
  }
}
