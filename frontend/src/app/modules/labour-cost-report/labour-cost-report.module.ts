import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LabourCostReportComponent } from './labour-cost-report.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { LabourCostReportRoutingModule } from './labour-cost-report-routing.module';

@NgModule({
  declarations: [LabourCostReportComponent],
  imports: [CommonModule, LabourCostReportRoutingModule, SharedModule],
})
export class LabourCostReportModule {}
