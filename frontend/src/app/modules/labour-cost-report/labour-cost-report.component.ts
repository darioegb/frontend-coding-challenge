import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { take } from 'rxjs/operators';
import {
  Column,
  OrderType,
  Sort,
  SortOption,
} from 'src/app/shared/models/table.model';
import { UtilService } from 'src/app/shared/services/util.service';
import { LabourStats, Provider } from './shared/labour-cost-report.model';
import { LabourCostReportService } from './shared/labour-cost-report.service';

@Component({
  selector: 'app-labour-cost-report',
  templateUrl: './labour-cost-report.component.html',
  styleUrls: ['./labour-cost-report.component.scss'],
})
export class LabourCostReportComponent implements OnInit {
  columns: Column<Provider>[] = [];
  data!: LabourStats;

  constructor(
    private labourCostReportService: LabourCostReportService,
    private translateService: TranslateService,
    private utilService: UtilService
  ) {}

  ngOnInit(): void {
    this.getTranslations();
    this.getData();
  }

  getTranslations(): void {
    this.translateService
      .get('labourStats.table.columns')
      .pipe(take(1))
      .subscribe((translations) => {
        const {
          name,
          workerCount,
          complianceScorePercentage,
          grossPayTotal,
          payrollAdminTotal,
          labourCostTotal,
          workForcePercentage,
        } = translations;
        this.columns = [
          {
            headerDef: 'name',
            cellDef: name,
            columnType: 'string',
            columnWidth: 'col-md-2',
          },
          {
            headerDef: 'workerCount',
            cellDef: workerCount,
            columnType: 'number',
          },
          {
            headerDef: 'complianceScorePercentage',
            cellDef: complianceScorePercentage,
            columnType: 'percentage',
          },
          {
            headerDef: 'grossPayTotal',
            cellDef: grossPayTotal,
            columnType: 'number',
            columnColor: 'dark',
            columnWidth: 'col-md-2',
            columnTextAlign: { header: 'text-center', body: 'text-end' },
          },
          {
            headerDef: 'payrollAdminTotal',
            cellDef: payrollAdminTotal,
            columnType: 'number',
            noZeroOrEmpty: true,
            columnTextAlign: { body: 'text-end' }
          },
          {
            headerDef: 'labourCostTotal',
            cellDef: labourCostTotal,
            columnType: 'number',
            columnWidth: 'col-md-2 offset-md-1',
          },
          {
            headerDef: 'workForcePercentage',
            cellDef: workForcePercentage,
            columnType: 'percentage',
          },
        ];
      });
  }

  getData(sort?: Sort<Provider>) {
    this.labourCostReportService.getAll().subscribe((data) => {
      const option: SortOption<Provider> = {
        key: 'name',
        reverse: sort && sort.order === OrderType.Descending,
      };
      data.providers = this.utilService.sortBy(data.providers, option);
      this.data = data;
    });
  }

  onSort(sort: Sort<Provider>) {
    if (!sort.column || sort.column === 'name') {
      this.getData(sort);
    } else {
      const directContractors = this.data.directContractors;
      if (
        directContractors &&
        !this.data.providers.includes(directContractors)
      ) {
        this.data.providers.push(directContractors);
        delete this.data.directContractors;
      }
      this.data.providers = this.utilService.sortBy(this.data.providers, {
        key: sort.column,
        reverse: sort.order === OrderType.Descending,
      });
    }
  }
}
