import { NgModule } from '@angular/core';
import { LabourCostReportComponent } from './labour-cost-report.component';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    component: LabourCostReportComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class LabourCostReportRoutingModule {}
